import re

def find_min(a):
    smallest = 1
    while True:
        if smallest in a:
            smallest+=1
        else:
            return smallest

def test():
    while True:
        a = input('Введите неотрицательные целые числа через пробел: ')
        if a!='cancel':
            a = re.findall('\d+', a)
            a = [int(x) for x in a]
            return find_min(set(a))
        else:
            break
        
test()