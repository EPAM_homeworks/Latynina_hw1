def find_palindrome():
    sum_of_pals = 0
    for num in range(1000001):
        if str(num)==str(num)[::-1] and "{0:b}".format(num)=="{0:b}".format(num)[::-1]:
            sum_of_pals += num
    return sum_of_pals
    
find_palindrome()