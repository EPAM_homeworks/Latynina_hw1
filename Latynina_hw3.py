import re
import itertools
a=0
str_nums=[]
while a!='cancel':
    a = input()
    if a!='cancel':
        str_nums.append(a)
for i in range(len(str_nums)):
    str_nums[i] = re.findall('\d+|-\d+', str_nums[i])
str_nums = list(itertools.chain.from_iterable(str_nums))
nums = [int(x) for x in str_nums]
print(sum(nums))