def count_frequency(a):
    d={}
    for word in a:
            if word not in d.keys():
                d[word]=1
            else:
                d[word]+=1
    return d

def f():
    while True:
        a = input('Введите текст: ').lower().split(' ')
        if a == ['cancel']:
            print('Bye!')
            break
        else:
            words = count_frequency(a)
            m = max(words.values())
            for k,v in words.items(): 
                if v == m:
                    print(f'{v} - {k}')
f()